import dotenv from "dotenv";
import mongoose from "mongoose";
import server from "./src/server";
import { LogError, LogSuccess } from "./src/utils/logger";

// * config the .env file
dotenv.config();

const PORT = process.env.PORT || 8000;

// TODO Mongoose Connection
async function dbConnect(): Promise<void>{
  const MONGO_URI = <string>'mongodb://192.168.0.103:27017/codeverification'
  try {
    await mongoose.connect(MONGO_URI)
    console.log('Mongodb connected')
    
    // Test connection by accessing a collection or document
    // const users = await mongoose.connection.db.collection('Users').find().toArray()
    // console.log('Number of Users:', users.length)
    // console.log(users)
  } catch (err) {
    console.error(err)
  }
}
dbConnect()

// * execute server
server.listen(PORT, () => {
  LogSuccess(`[SERVER ON]: Running on http://localhost:${PORT}/api`);
});

// * control server error
server.on("error", (error: any) => {
  LogError(`[SERVER ERROR]: ${error}`);
});
