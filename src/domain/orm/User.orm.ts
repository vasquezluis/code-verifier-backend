import userModel from "../entities/User.entity";
import { LogError, LogSuccess } from "../../utils/logger";

// PETICIONES CRUD

/**
 * * Method to obtain all users from colletion 'Users' in mongo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {
  try {
    // * search all users
    const response = await userModel.find({});
    return response;
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all Users: ${error}`);
  }
};

// TODO
// GET user by Id
export const getUserById = async (id: string): Promise<any | undefined> => {
  try {
    const response = await userModel.findById(id);

    return response;
  } catch (error) {
    LogError(`[ORM ERROR]: Getting user by Id: ${error}`);
  }
};

// GET user by email

// DELETE user by ID
export const deleteUserById = async (id: string): Promise<any | undefined> => {
  try {
    const response = await userModel.findByIdAndDelete(id);

    return response;
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting user by Id: ${error}`);
  }
};

// CREATE new user
export const createUser = async (user: any): Promise<any | undefined> => {
  try {
    const response = await userModel.create(user);

    return response;
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user: ${error}`);
  }
};

// UPDATE user by ID
export const updateUserById = async (id: string, user: any): Promise<any | undefined> => {
  try {
    const response = await userModel.findByIdAndUpdate(id, user)

    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Creating user: ${error}`);
  }
};
