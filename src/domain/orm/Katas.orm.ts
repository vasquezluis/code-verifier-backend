import { KatasEntity } from "../entities/Katas.entity";
import { LogError, LogSuccess } from "@/utils/logger";

// PETICIONES CRUD

/**
 * * Method to obtain all users from colletion 'Users' in mongo Server
 */
export const GetAllKatas = async (): Promise<any[] | undefined> => {
  try {
    let katasModel = KatasEntity();

    // * search all users
    return katasModel.find({ isDelete: false });
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all Katas: ${error}`);
  }
};

// TODO 
// GET katas by Id
// DELETE kata by ID
// CREATE new kata 
// UPDATE kata by ID