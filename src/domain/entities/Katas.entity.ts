import mongoose from "mongoose";

export const KatasEntity = (): any => {
  let katasSchema = new mongoose.Schema({
    name: String,
    description: String,
    level: Number,
    user: String,
    date: Date,
    valoration: {
      type: Number,
      enum: [1, 2, 3, 4, 5],
    },
    chances: Number,
  });

  return mongoose.model("Katas", katasSchema);
};
