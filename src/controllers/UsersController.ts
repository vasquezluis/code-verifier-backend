import { Get, Delete, Route, Tags, Query, Post, Put } from "tsoa";
import { IUserControllers } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

// ORM -> users collection
import {
  getAllUsers,
  getUserById,
  deleteUserById,
  createUser,
  updateUserById,
} from "../domain/orm/User.orm";

@Route("/api/users")
@Tags("UserController")
export class UserController implements IUserControllers {
  /**
   * Endpoint to retrieve the users in the collection 'Users' of db
   * @param {string} id Id of user to retrieve (optinal)
   * @returns  All users or user found by Id
   */
  @Get("/")
  public async getUsers(@Query() id?: string): Promise<any> {
    try {
      let response: any = "";

      if (id) {
        LogSuccess(`[/api/users] Get user by Id ${id}`);
        response = await getUserById(id);
      } else {
        LogSuccess("[/api/users] Get all users Request");
        response = await getAllUsers();
      }

      return response;
    } catch (error) {
      if (error instanceof Error) {
        console.log(error.message);
      }
    }
  }

  /**
   * Endpoint to delete the users in the collection 'Users' of DB
   * @param {string} id Id of user to delete
   * @returns Message informing if the deletion was succesfully
   */
  @Delete("/")
  public async deleteUser(@Query() id?: string): Promise<any> {
    try {
      let response: any = "";

      if (id) {
        LogSuccess(`[/api/users] Delete user by Id ${id}`);
        await deleteUserById(id).then((r) => {
          response = {
            message: `User with id ${id} deleted succesfully!`,
          };
        });
      } else {
        LogWarning("[/api/users] Delete user request without Id");
        response = {
          message: "Please, provide and Id to delete a User",
        };
      }

      return response;
    } catch (error) {
      if (error instanceof Error) {
        console.log(error.message);
      }
    }
  }

  /**
   *
   * @param user
   * @returns
   */
  @Post("/")
  public async createUser(user: any): Promise<any> {
    let response: any = "";

    LogSuccess(`[/api/users] Create user ${user}`);

    await createUser(user).then((r) => {
      response = {
        message: `User created: ${user.name}`,
      };
    });

    return response;
  }

  /**
   *
   * @param id
   * @param user
   */
  @Put("/")
  public async updateUser(@Query() id: string, user: any): Promise<any> {
    try {
      let response: any = "";

      if (id) {
        LogSuccess(`[/api/users] Update user by Id ${id}`);
        await updateUserById(id, user).then((r) => {
          response = {
            message: `User with id ${id} updated succesfully!`,
          };
        });
      } else {
        LogWarning("[/api/users] Update user request without Id");
        response = {
          message: "Please, provide and Id to update a User",
        };
      }

      return response;
    } catch (error) {
      if (error instanceof Error) {
        console.log(error.message);
      }
    }
  }
}
