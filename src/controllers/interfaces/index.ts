import { BasicResponse, MessageDateResponse } from "../types";

export interface IHelloController {
  getMessage(name?: string): Promise<BasicResponse>;
}

export interface IGoodbyeController {
  getMessage(name?: string, date?: string): Promise<MessageDateResponse>;
}

export interface IUserControllers {
  // all users from database || get user by Id
  getUsers(id?: string): Promise<any>
  
  // delete user
  deleteUser(id?: string): Promise<any>

  // create user
  createUser(user: any): Promise<any>

  // update user
  updateUser(id: string, user: any): Promise<any>
}