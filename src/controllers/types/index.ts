/**
 * Basic JSON response for controllers
 */
export type BasicResponse = {
  message: string;
};

/**
 * message, date JSON response for controllers
 */
export type MessageDateResponse = {
  message: string;
  date: string;
};

/**
 * Error JSON response for controllers
 */
export type ErrorResponse = {
  error: string;
};
