import express, { Express, Request, Response } from "express";

// * swagger
import swaggerUI from 'swagger-ui-express'

// * security
import cors from "cors";
import helmet from "helmet";

// TODO HTTPS

// * Root routes
import rootRouter from "../routes";

// * create Express app
const server: Express = express();

// * swagger config and route
server.use('/docs', swaggerUI.serve, swaggerUI.setup(undefined, {
  swaggerOptions: {
    url: '/swagger.json',
    explorer: true
  }
}))

// * Define Server to use "/api" and use rootRouter from 'index.ts' in routes
// from this point onover: http://localhost:8000/api/...
server.use("/api", rootRouter);

// static server
server.use(express.static("public"));

// * security config
server.use(helmet());
server.use(cors());

// * content type config
server.use(express.urlencoded({ extended: true, limit: "50mb" }));
server.use(express.json({ limit: "50mb" }));

// * redirections
// http://localhost:8000/ -> http://localhost:8000/api/
server.get("/", (req: Request, res: Response) => {
  res.redirect("/api");
});

export default server;
