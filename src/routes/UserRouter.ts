import express, { Request, Response } from "express";
import { UserController } from "../controllers/UsersController";
import { LogInfo } from "../utils/logger";

let usersRouter = express.Router();

// http:localhost:8000/api/users?id=dafsdfbasd
usersRouter
  .route("/")
  // GET -> http:localhost:8000/api/hello?name=Martin/
  .get(async (req: Request, res: Response) => {
    // Obtain a query param (id)
    let id: any = req?.query?.id;
    LogInfo(`Query param ${id}`);

    // controller instance to execute a method
    const controller: UserController = new UserController();
    // get response
    const response: any = await controller.getUsers(id);

    // send the response to the client
    return res.send(response);
  })
  // DELETE:
  .delete(async (req: Request, res: Response) => {
    // Obtain a query param (id)
    let id: any = req?.query?.id;
    LogInfo(`Query param ${id}`);

    // controller instance to execute a method
    const controller: UserController = new UserController();
    // get response
    const response: any = await controller.deleteUser(id);

    // send the response to the client
    return res.send(response);
  })
  // POST:
  .post(async (req: Request, res: Response) => {
    let name: any = req.query?.name;
    let email: any = req.query?.email;
    let age: any = req.query?.age;

    let user = {
      name,
      email,
      age,
    };

    // controller instance to execute a method
    const controller: UserController = new UserController();
    // get response
    const response: any = await controller.createUser(user);

    // send the response to the client
    return res.send(response);
  })
  // PUT:
  .put(async (req: Request, res: Response) => {
    // Obtain a query param (id)
    let id: any = req?.query?.id;

    let name: any = req.query?.name;
    let email: any = req.query?.email;
    let age: any = req.query?.age;
    LogInfo(`Query param ${id} ${name} ${age} ${email}`);

    let user = {
      name,
      email,
      age,
    };

    // controller instance to execute a method
    const controller: UserController = new UserController();
    // get response
    const response: any = await controller.updateUser(id, user);

    // send the response to the client
    return res.send(response);
  });

export default usersRouter;
