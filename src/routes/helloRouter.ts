import express, { Request, Response } from "express";
import { HelloController } from "../controllers/helloController";
import { LogInfo } from "../utils/logger";
import { BasicResponse } from "@/controllers/types";

// Router from express
let helloRouter = express.Router();

// http:localhost:8000/api/hello?name=Martin/
helloRouter
  .route("/")
  // GET -> http:localhost:8000/api/hello?name=Martin/
  .get(async (req: Request, res: Response) => {
    // Obtain a query param
    let name: any = req?.query?.name;
    LogInfo(`Query param ${name}`);

    // controller instance to execute a method
    const controller: HelloController = new HelloController();
    // get response
    const response: BasicResponse = await controller.getMessage(name);

    // send the response to the client
    return res.json({ message: response.message });
  });

export default helloRouter;
