import express, { Request, Response } from "express";
import { GoodbyeController } from "../controllers/goodbyeController";
import { LogInfo } from "../utils/logger";

// Router from express
let goodbyeRouter = express.Router();

// http:localhost:8000/api/goodbye?name=Martin/
goodbyeRouter
  .route("/")
  // GET -> http:localhost:8000/api/hello?name=Martin/
  .get(async (req: Request, res: Response) => {
    // Obtain a query param
    let name: any = req?.query?.name;
    LogInfo(`Query param ${name}`);

    // controller instance to execute a method
    const controller: GoodbyeController = new GoodbyeController();
    // get response
    const response = await controller.getMessage(name);

    // send the response to the client
    return res.json({ message: response.message, date: response.date });
  });

export default goodbyeRouter;
