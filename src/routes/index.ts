/**
 * Root Router
 * Redirection to routers
 */

import express, { Response, Request } from "express";
import helloRouter from "./helloRouter";
import goodbyeRouter from "./goodbyeRouter";
import { LogInfo } from "../utils/logger";
import usersRouter from "./UserRouter";

// server instance
let server = express();

// router instance
let rootRouter = express.Router();

// Activate for requrest to http://localhost:8000/api

// GET => http://localhost:8000/api/
rootRouter.get("/", (req: Request, res: Response) => {
  LogInfo("GET: http://localhost:8000/api/");

  res.status(200).json({ data: { message: `Hola, que hace` } });
});

// redirections to routes & controllers
server.use("/", rootRouter); // http://localhost:8000/api/
server.use("/hello", helloRouter); // http://localhost:8000/api/hello --> helloRouter
server.use("/goodbye", goodbyeRouter); // http://localhost:8000/api/goodbye --> goodbyeRouter
// add more routes
server.use("/users", usersRouter); // http://localhost:8000/api/users --> usersRouter

export default server;
